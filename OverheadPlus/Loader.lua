--[[
	You can find the main module at https://www.roblox.com/library/11217125583/
	Otherwise you can change the assetId variable to make your own fork of this code!
]]


local assetId = 11217125583
local asset = nil

if game:GetService("ServerStorage"):FindFirstChild("OverheadPlus") then
	asset = require(game:GetService("ServerStorage"):FindFirstChild("OverheadPlus"))
else
	asset = require(assetId)
end

asset:Initate(script.Parent.Parent)
